module Simple
where

-- Definieren Sie eine Funktion fib zur Berechung der Fibonacci-Zahlen
-- ab 0 
fib     :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib x = fib (x-1) + fib (x-2)


-- Definieren Sie eine Funktion fib zur Berechung der Fibonacci-Zahlen
-- ab 0 mit linearer Laufzeit

fib2    :: Integer -> Integer
fib2 0 = 0
fib2 1 = 1
fib2 x = fib' 0 1 x
	where
		fib' n0 n1 x 
			| x == 0 = n0
			| x > 0 = fib' n1 (n0+n1) (x-1)


-- Definieren Sie eine Funktion c (für Collatz), die berechnet
-- wie viele Rekursionsschritte benötigt werden, um
-- eine natürliche Zahl n >= 1 auf 1 zu
-- reduzieren.
--
-- Folgende Reduktionsregel sind dabei anzuwenden: Wenn n gerade ist,
-- so wird n halbiert, wenn n ungerade ist, so wird n verdreifacht und um
-- 1 erhöht.

c       :: Integer -> Integer
c n = c' n
	where 
		c' 1 = 0
		c' n 
			| even n = c' (n `div` 2) +1
			| odd  n = c' (n*3 +1) +1

-- Definieren Sie ein endrekurive Variante von c
    
c1       :: Integer -> Integer
c1 = c' 0 
	where 
		c' cnt 1 = cnt
		c' cnt n 
			| even n = c' (cnt+1) (n `div` 2) 
			| odd  n = c' (cnt+1) (n*3 +1)


-- Definieren Sie eine Funktion cmax, die für ein
-- Intervall von Zahlen das Maximum der
-- Collatz-Funktion berechnet. Nutzen Sie die
-- vordefinierten Funkt min und max.

cmax    :: Integer -> Integer -> Integer
cmax lb ub 
	| lb == ub = c lb
	| lb < ub = max (c lb) (cmax (lb+1) ub) 
	-- | otherwise error


-- Definieren Sie eine Funktion imax, die für ein
-- Intervall von Zahlen das Maximum einer
-- ganzzahligen Funktion berechnet. Formulieren
-- Sie die obige Funktion cmax so um, dass sie mit imax arbeitet.

imax    :: (Integer -> Integer) -> Integer -> Integer -> Integer
imax f lb ub 
	| lb == ub = f lb
	| lb < ub = max (f lb) (imax f (lb+1) ub)
	-- | otherwise error


cmax1   :: Integer -> Integer -> Integer
cmax1
    = imax c

-- Entwickeln Sie eine Funktion,
-- die die Position und den Wert bestimmt, an der
-- das Maximum angenommen wird.
-- Versuchen Sie, eine endrekursive Lösung zu finden
-- (mit einer lokalen Hilfsfunktion).

imax2   :: (Integer -> Integer) -> Integer -> Integer -> (Integer, Integer)
imax2 = imax2' 0
	where
		imax2' pos f lb ub 
						  | lb == ub = (pos, f lb)
						  | lb <  ub = if val > v1 then (pos, val) else (p1,v1)
						    where
						    (p1, v1) = imax2' (pos+1) f (lb+1) ub
						    val = f lb

-- ----------------------------------------
cmax2   :: Integer -> Integer -> (Integer, Integer)
cmax2
    = imax2 c
