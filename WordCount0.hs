{-# LANGUAGE UnboxedTuples #-}

module Main where

-- the working horse
-- with predefined type Sum for (+,0) monoid 
import           Data.Monoid

-- the more effizent Text representation
-- than the native String type
import qualified Data.Text          as T
import qualified Data.Text.IO       as T
-- import qualified Prelude            as P

import           System.Environment (getArgs)

-- ----------------------------------------
--
-- the whole main program

main :: IO ()
main
  = do (inp :_) <- getArgs
       text     <- T.readFile inp
       writeResult inp (processText text)
       return ()

-- --------------------

type Counters
  = (Sum Int,              -- line count
     (Sum Int,             -- word count
      (Sum Int,            -- char count
       ())))

-- --------------------
--
-- the whole computation

processText :: T.Text -> Counters
processText t
  = mconcat . map toCounters $ T.lines t

-- process a single line
{- lange Version
toCounters :: T.Text -> Counters
toCounters t = (1, (words'', (chars', ())))
  where
    (words', chars', lastChar') 
            = T.foldr 
                (\ c (wCnt, cCnt, lc) -> (wCnt + (isWord c) lc, cCnt +1, c) ) 
                (1, 1, '\n') 
                t
    words'' 
            | isSpace lastChar' = words' -1
            | otherwise         = words'
    isWord c lc
            | (isSpace c) && (not (isSpace lc)) = 1
            | otherwise                         = 0
    isSpace c = c == '\t' || c == '\n' || c == ' '
-}
-- kurze Version
toCounters :: T.Text -> Counters
toCounters line = (Sum 1,
                    (Sum . length . T.words $ line,
                      (Sum . (1+) $ T.length $ line,
                        ())))   

-- --------------------
--
-- the boring formatting of the results

writeResult :: String -> Counters -> IO ()
writeResult f (Sum lc, (Sum wc, (Sum cc, ())))
  = putStrLn $ unlines $
    [ "Statistics for " ++ show f
    , "lines        : " ++ fillI8 lc
    , "words        : " ++ fillI8 wc
    , "chars        : " ++ fillI8 (cc + lc)
    ]
  where
    fillI8 = fillLeft 8 . show

    fillLeft n v
      = replicate ((n - m) `max` 0) ' ' ++ v
        where
          m = length v

-- ----------------------------------------
