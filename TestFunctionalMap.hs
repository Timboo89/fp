module TestFunctionalMap
where

import           Data.FunctionalMap
import           Prelude            hiding (lookup)
import qualified Prelude            as P
import           Test.QuickCheck

-- alle Elemente enthalten?
prop_1 :: String -> Bool
prop_1 xs 
    = foldr (\ x res -> (isIn (lookup x tree)) P.&& res) True xs 
    where
        tree = fromList P.$ toTuple xs

-- alle Elemente eingefügt und hälfte gelöscht. Nicht enthalten?
prop_2 :: String -> Bool
prop_2 xs 
    = foldr (\ x res -> not (isIn (lookup x dTree)) P.&& res) True dxs
    where
        tree = fromList P.$ toTuple xs
        dxs = take (length xs `div` 2) xs
        dTree = foldr (\ x res -> delete x res) tree dxs 

-- Liste in zwei Hälften getrennt. Beide Listen zu Trees konvertiert. Bäume zusammengefügt. Alle Elemente enthalten?
prop_3 :: String -> Bool
prop_3 xs 
    = foldr (\ x res -> (isIn (lookup x tree)) P.&& res) True xs 
    where
        (xs1, xs2) = P.splitAt (P.length xs `div` 2) xs
        tree1 = fromList P.$ toTuple xs1
        tree2 = fromList P.$ toTuple xs2
        tree  = union tree2 tree1


-- ----------------------------------------

toTuple :: [a] -> [(a,a)]
toTuple = P.map (\ x -> (x,x)) 

isIn :: Maybe a -> Bool
isIn Nothing  = False
isIn (Just _) = True

-- ----------------------------------------

quickCheck' :: Testable prop => prop -> IO ()
quickCheck' = quickCheckWith stdArgs{maxSuccess=2000}

main
  = do quickCheck'  prop_1
       quickCheck'  prop_2
       quickCheck'  prop_3
       
-- ----------------------------------------

