module Data.IntervalSet
where

-- ----------------------------------------

-- an pair of Ints can represent closed Intervals
-- (i, j) <=> [i..j]
-- Intervalls with i > j represent the empty set

type Interval = (Int, Int)

overlap :: Interval -> Interval -> Bool
overlap a@(x1, y1) b@(x2, y2)
    | x1 > x2        = overlap b a
    | not (less a b) = True
    | otherwise      = False
 

less :: Interval -> Interval -> Bool
less (_x1, y1) (x2, _y2)
  = x2 > (y1+1)

                           
emptyInterval :: Interval -> Bool
emptyInterval (x, y)
  = x > y


-- merge 2 (overlapping) intervals
merge :: Interval -> Interval -> Interval
merge (x1, y1) (x2, y2) = (x1 `min` x2, y1 `max` y2)


-- ----------------------------------------

-- a set of integers can be represented by an
-- ordered list of none empty intervals, which
-- do not overlap

type IntervalSet = [Interval]

inv :: IntervalSet -> Bool
inv [ ] = True
inv [x] = not $ emptyInterval x
inv (x1:x2:xs) = (not $ emptyInterval x1) 
              && (less x1 x2) 
              && (not $ overlap x1 x2) 
              && inv (x2:xs)


-- ----------------------------------------
-- internal interval set ops

singleInterval :: Int -> Int -> IntervalSet
singleInterval x y
    | x <= y    = [(x, y)]
    | otherwise = []

insertInterval :: Interval -> IntervalSet -> IntervalSet
insertInterval val          [ ]   = [val]
insertInterval val@(v1, v2) [x] 
    | less val x = val : [x]
    | less x val = x : [val]
    | otherwise  = [merge val x]
insertInterval val@(v1, v2) xs'@(x:xs) 
    | less val x = val : xs'
    | less x val = x : insertInterval val xs
    | otherwise  = insertInterval (merge val x) xs


fromIntervalList :: [(Int, Int)] -> IntervalSet
fromIntervalList = foldr func []
    where
        func x res 
            | emptyInterval x = res
            | otherwise       = insertInterval x res


-- ----------------------------------------
--
-- exported ops, names similar to Data.Set

empty :: IntervalSet
empty = []

singleton :: Int -> IntervalSet
singleton i = singleInterval i i

insert :: Int -> IntervalSet -> IntervalSet
insert i = insertInterval (i, i)

-- spec
union' :: IntervalSet -> IntervalSet -> IntervalSet
union' a b = fromIntervalList (a++b) 


union :: IntervalSet -> IntervalSet -> IntervalSet
union            xs         []      = xs
union            []         ys      = ys
union xs'@(x@(_x1,x2):xs) ys'@(y@(_y1,y2):ys)
    | less x y  = x : union xs  ys'
    | less y x  = y : union xs' ys
    | otherwise = merging
    where 
        merged = merge x y
        merging 
            | x2 > y2   = union (merged:xs) ys
            | otherwise = union xs (merged:ys)


member :: Int -> IntervalSet -> Bool
member i = foldr (\ (x,y) res -> ((i >= x) && (i <= y)) || res) False

         
fromList :: [Int] -> IntervalSet
fromList = foldr (\ i res -> insert i res) []


toList :: IntervalSet -> [Int]
toList = foldr (\ (x,y) res -> [x..y] ++ res) [] 


-- ----------------------------------------
