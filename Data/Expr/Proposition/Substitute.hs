module Data.Expr.Proposition.Substitute where

import Data.List      (union, nub)

import Data.Expr.Proposition.Types

-- ----------------------------------------
-- variable substitution

type VarEnv = [(Ident, Expr)]


errMsg_NoVar :: Show a => a -> String
errMsg_NoVar x = "No value for variable '" ++ show x ++ "'' defined in envirnment."

substVars :: VarEnv -> Expr -> Expr
substVars env = substVars'
    where
        substVars' l@(Lit _)         = l
--        substVars' (Var k)           = maybe (error (errMsg_NoVar k)) id . lookup k $ env
        substVars' (Var k)           = maybe (Var k) id . lookup k $ env
        substVars' (Unary op e)      = Unary op $ substVars' e
        substVars' (Binary op e1 e2) = Binary op (substVars' e1) $ substVars' e2

freeVars :: Expr -> [Ident]
freeVars = nub . freeVars'
    where
        freeVars' :: Expr -> [Ident]
        freeVars' (Lit _)        = [] 
        freeVars' (Var k)        = [k]
        freeVars' (Unary _ x)    = freeVars' x
        freeVars' (Binary _ x y) = freeVars' x ++ freeVars' y

-- ----------------------------------------
