-- | the visitor pattern for Expr

module Data.Expr.Proposition.Visit where

import Data.Expr.Proposition.Types

-- ----------------------------------------

data Visitor r
  = V { vLit    :: Bool  -> r
      , vVar    :: Ident -> r
      , vUnary  :: Op1   -> r -> r
      , vBinary :: Op2   -> r -> r -> r
      }

idExpr :: Visitor Expr
idExpr
  = V { vLit = Lit
      , vVar = Var
      , vUnary = Unary
      , vBinary = Binary
      }

visit :: Visitor r -> Expr -> r
visit v = visit'
    where
        visit' (Lit l)           = vLit    v l
        visit' (Var k)           = vVar    v k
        visit' (Unary op e)      = vUnary  v op $ visit' e
        visit' (Binary op e1 e2) = vBinary v op (visit' e1) $ visit' e2

-- ----------------------------------------
