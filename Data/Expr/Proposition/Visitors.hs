-- | the visitor pattern for Expr

module Data.Expr.Proposition.Visitors where

import Data.Expr.Proposition.Types
import Data.Expr.Proposition.Visit
import Data.Expr.Proposition.Eval (mf1, mf2)

import Data.Set(Set)
import qualified Data.Set as S

-- ----------------------------------------

type Idents = Set Ident

freeVars :: Expr -> Idents
freeVars
  = visit $
    V { vLit    = \ _ -> S.empty
      , vVar    = S.singleton
      , vUnary  = \ _ -> id
      , vBinary = \ _ -> S.union
      }
    
type VarEnv = [(Ident, Expr)]

errMsg_NoVar :: Show a => a -> String
errMsg_VarsForbidden :: String
errMsg_NoVar x = "No value for variable '" ++ show x ++ "'' defined in envirnment."
errMsg_VarsForbidden = "Function 'Data.Expr.Proposition.Visitors.eval' doesn't support expressions with variables."


substVars :: VarEnv -> Expr -> Expr
substVars env
  = visit $ idExpr { vVar = \ k -> maybe (Var k) id . lookup k $ env }
--  = visit $ idExpr { vVar = \ k -> maybe (error (errMsg_NoVar k)) id . lookup k $ env }
--    V { vLit    = Lit
--      , vVar    = \ k -> maybe (error (errMsg_NoVar k)) id . lookup k $ env
--      , vUnary  = Unary
--      , vBinary = Binary
--      }

eval :: Expr -> Bool
eval
  = visit $
    V { vLit    = id
      , vVar    = error errMsg_VarsForbidden
      , vUnary  = \ op x -> uf op x 
      , vBinary = \ op x y -> bf op x y
      }
    where
        uf :: Op1 -> (Bool -> Bool)
        uf Not = not
        bf :: Op2 -> (Bool -> Bool -> Bool)
        bf And   = (&&)
        bf Or    = (||)
        bf Impl  = \ x y -> (not x) || y
        bf Xor   = \ x y -> (x || y) && (not (x && y))
        bf Equiv = (==)


-- ----------------------------------------
